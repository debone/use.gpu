declare module "@use-gpu/wgsl/instance/vertex/light.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getLightVertex: ParsedBundle;
  export default __module;
}
