declare module "@use-gpu/wgsl/layout/rectangle.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const transformRectangle: ParsedBundle;
  export default __module;
}
