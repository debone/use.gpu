import { RawLines } from '../primitives/raw-lines';

/** Draws line segments. */
export const LineLayer = RawLines;